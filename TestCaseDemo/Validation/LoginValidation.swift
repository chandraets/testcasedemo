//
//  LoginValidation.swift
//  TestCaseDemo
//
//  Created by Puja Bhagat on 05/03/2022.
//

import Foundation

struct LoginValidation {
    func validate(request: LoginRequest) -> ValidationResponse {
        guard !request.userEmail.isEmpty && !request.userPassword.isEmpty else {
            return ValidationResponse(message: "Email or password can not be empty", isValid: false)
        }
        
        guard request.userEmail.isValidEmail() else {
            return ValidationResponse(message: "Email id is invalid", isValid: false)
        }
        return ValidationResponse(message: nil, isValid: true)
    }
}
