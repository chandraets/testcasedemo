//
//  ValidationResponse.swift
//  TestCaseDemo
//
//  Created by Puja Bhagat on 05/03/2022.
//

import Foundation

struct ValidationResponse {
    let message: String?
    let isValid: Bool
}
