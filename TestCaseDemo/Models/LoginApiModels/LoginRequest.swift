//
//  LoginRequest.swift
//  TestCaseDemo
//
//  Created by Puja Bhagat on 05/03/2022.
//

import Foundation

struct LoginRequest: Encodable {
    let userEmail, userPassword: String
}
