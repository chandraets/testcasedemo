//
//  LoginData.swift
//  TestCaseDemo
//
//  Created by Puja Bhagat on 05/03/2022.
//

import Foundation

struct LoginData {
    let errorMessage: String?
    let response: LoginResponse?
}
