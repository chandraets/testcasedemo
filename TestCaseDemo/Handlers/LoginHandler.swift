//
//  LoginHandler.swift
//  TestCaseDemo
//
//  Created by Puja Bhagat on 05/03/2022.
//

import Foundation

struct LoginHandler {
    private let validation = LoginValidation()
    private let loginApiResource = LoginApiResource()
    
    func authenticateUser(request: LoginRequest, completionHandler: @escaping(_ loginData: LoginData?) -> ()) {
        let validationResult = validation.validate(request: request)
        if(validationResult.isValid){
            loginApiResource.authenticateUser(request: request) { (loginResponse) in
                completionHandler(LoginData(errorMessage: nil, response: loginResponse))
            }
        }
        completionHandler(LoginData(errorMessage: validationResult.message, response: nil))
    }
}
