//
//  LoginValidationUnitTests.swift
//  TestCaseDemoTests
//
//  Created by Puja Bhagat on 05/03/2022.
//

import XCTest
@testable import TestCaseDemo

class LoginValidationUnitTests: XCTestCase {

    func test_LoginValidation_With_EmptyStrings_Returns_Validation_Failure(){
        // ARRANGE
        let loginValidation = LoginValidation()
        let request = LoginRequest(userEmail: "", userPassword: "")
        
        // ACT
        let validationResponse = loginValidation.validate(request: request)
        
        // ASSERT
        XCTAssertFalse(validationResponse.isValid)
        XCTAssertNotNil(validationResponse.message)
        XCTAssertEqual(validationResponse.message, "Email or password can not be empty")
    }
    
    func test_LoginValidation_With_EmptyEmail_Returns_Validation_Failure(){
        // ARRANGE
        let loginValidation = LoginValidation()
        let request = LoginRequest(userEmail: "", userPassword: "test")
        
        // ACT
        let validationResponse = loginValidation.validate(request: request)
        
        // ASSERT
        XCTAssertFalse(validationResponse.isValid)
        XCTAssertNotNil(validationResponse.message)
        XCTAssertEqual(validationResponse.message, "Email or password can not be empty")
    }
    
    func test_LoginValidation_With_EmptyPassword_Returns_Validation_Failure(){
        // ARRANGE
        let loginValidation = LoginValidation()
        let request = LoginRequest(userEmail: "test", userPassword: "")
        
        // ACT
        let validationResponse = loginValidation.validate(request: request)
        
        // ASSERT
        XCTAssertFalse(validationResponse.isValid)
        XCTAssertNotNil(validationResponse.message)
        XCTAssertEqual(validationResponse.message, "Email or password can not be empty")
    }
    
    func test_LoginValidation_With_InValidEmail_Returns_Validation_Failure(){
        // ARRANGE
        let loginValidation = LoginValidation()
        let request = LoginRequest(userEmail: "chandra.ets", userPassword: "test")
        
        // ACT
        let validationResponse = loginValidation.validate(request: request)
        
        // ASSERT
        XCTAssertFalse(validationResponse.isValid)
        XCTAssertNotNil(validationResponse.message)
        XCTAssertEqual(validationResponse.message, "Email id is invalid")
    }
    
    func test_LoginValidation_With_ValidRequest_Returns_Validation_Success(){
        // ARRANGE
        let loginValidation = LoginValidation()
        let request = LoginRequest(userEmail: "chandra.ets@gmail.com", userPassword: "test")
        
        // ACT
        let validationResponse = loginValidation.validate(request: request)
        
        // ASSERT
        XCTAssertTrue(validationResponse.isValid)
        XCTAssertNil(validationResponse.message)
    }
}
